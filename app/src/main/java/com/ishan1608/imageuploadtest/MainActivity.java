package com.ishan1608.imageuploadtest;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    private Button buttonChoose;
    private Button buttonUpload;

    private ImageView imageView;

    private Bitmap bitmap;

    private int PICK_IMAGE_REQUEST = 1;
    private int PICK_VIDEO_REQUEST = 2;

//    private String UPLOAD_URL ="http://requestb.in/y4sk6ny4";
    private File fileObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);

        imageView  = (ImageView) findViewById(R.id.imageView);

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }

    private void showImageFileChooser() {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    private void showVideoFileChooser() {
        Intent intent = new Intent();
        // Show only video, no images or anything else
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO_REQUEST);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*// Image choose reaction section
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                Uri filePath = data.getData();
                //Getting the Bitmap from Gallery
//                bitmap = BitmapFactory.decodeFile(fileObject.getPath());
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                //Setting the Bitmap to ImageView
//                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                // Creating a File object
                PackageManager mPackageManager = getPackageManager();
                String packageName = getPackageName();
                PackageInfo packageInfo = mPackageManager.getPackageInfo(packageName, 0);
                packageName = packageInfo.applicationInfo.dataDir;

                fileObject = new File(packageName, "hello");
                Toast.makeText(getBaseContext(), fileObject.getPath(), Toast.LENGTH_LONG).show();
                OutputStream os;
                os = new BufferedOutputStream(new FileOutputStream(fileObject));
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.close();

                imageView.setImageBitmap(BitmapFactory.decodeFile(fileObject.getPath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "FileNotFoundException", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "IOException", Toast.LENGTH_SHORT).show();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "NameNotFoundException", Toast.LENGTH_SHORT).show();
            }
        }*/

        // Video choose reaction section
        if (requestCode == PICK_VIDEO_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Log.d("VIDEO_REQUEST", "Video request code confirmed");
            try {
                Uri filePathUri = data.getData();
                String realPath = getPath(getBaseContext(), filePathUri);
                Log.d("REAL_PATH", realPath);

                fileObject = new File(realPath);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Start of getting file path
    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    // End of getting file path

    @Override
    public void onClick(View v) {
        /*// Image Upload Section
        if(v == buttonChoose){
            showImageFileChooser();
        }

        if(v == buttonUpload){
            Toast.makeText(getBaseContext(), "Creating Request", Toast.LENGTH_SHORT).show();
            // Not sure what is going on here
            HashMap<String, String> params = new HashMap<>();

            params.put("venue_id", "1");
            params.put("action", "add");
            params.put("a_session", "159");
            params.put("a_source",  "VENDOR_APP");


            String UPLOAD_URL = "http://api.weddingonclick.com/vendor/venue-images/";
            MultipartRequest mr = new MultipartRequest(UPLOAD_URL, new Response.Listener<String>(){

                @Override
                public void onResponse(String response) {
                    Log.d("response", response);
                    Toast.makeText(getBaseContext(), "Response : " + response, Toast.LENGTH_LONG).show();
                }

            }, new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Volley Request Error", error.getLocalizedMessage());
                    Toast.makeText(getBaseContext(), "Request error " + error.getMessage(), Toast.LENGTH_LONG).show();
                }

            }, fileObject, params);
            mr.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(mr);
            Toast.makeText(getBaseContext(), "Request started", Toast.LENGTH_SHORT).show();
        }*/
        // Video Upload section
        if(v == buttonChoose){
            showVideoFileChooser();
        }

        if(v == buttonUpload) {
            // Video Upload section
            Toast.makeText(getBaseContext(), "Creating Request", Toast.LENGTH_SHORT).show();
            // Not sure what is going on here
            HashMap<String, String> params = new HashMap<>();

            params.put("package_id", "1");
            params.put("a_session", "159");
            params.put("a_source",  "VENDOR_APP");


            String UPLOAD_URL = "http://requestb.in/1lzf7cz1";
            VideoMultipartRequest vmr = new VideoMultipartRequest(UPLOAD_URL, new Response.Listener<String>(){

                @Override
                public void onResponse(String response) {
                    Log.d("response", response);
                    Toast.makeText(getBaseContext(), "Response : " + response, Toast.LENGTH_LONG).show();
                }

            }, new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Volley Request Error", error.getLocalizedMessage());
                    Toast.makeText(getBaseContext(), "Request error " + error.getMessage(), Toast.LENGTH_LONG).show();
                }

            }, fileObject, params);
            vmr.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(vmr);
            Toast.makeText(getBaseContext(), "Request started", Toast.LENGTH_SHORT).show();
        }
    }

    /*@SuppressWarnings("deprecation")
    public class MultipartRequest extends Request<String> {

        private MultipartEntity entity = new MultipartEntity();

        private static final String FILE_PART_NAME = "image";

        private final Response.Listener<String> mListener;
        private final File file;
        private final HashMap<String, String> params;

        public MultipartRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener, File file, HashMap<String, String> params) {
            super(Method.POST, url, errorListener);

            mListener = listener;
            this.file = file;
            this.params = params;
            buildMultipartEntity();
        }

        private void buildMultipartEntity() {
            entity.addPart(FILE_PART_NAME, new FileBody(file));
            try {
                for (String key : params.keySet()) {
                    entity.addPart(key, new StringBody(params.get(key)));
                }
            } catch (UnsupportedEncodingException e) {
                VolleyLog.e("UnsupportedEncodingException");
            }
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String,String> headers = new HashMap<>();
            headers.put("Content-Type", "multipart/form-data");
            headers.put("userid", "8");
            headers.put("authorization", "V2kPVYlJIRDmJfeN");
            return headers;
        }

        @Override
        public String getBodyContentType() {
            return entity.getContentType().getValue();
        }

        @Override
        public byte[] getBody() throws AuthFailureError {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                entity.writeTo(bos);
            } catch (IOException e) {
                VolleyLog.e("IOException writing to ByteArrayOutputStream");
            }
            return bos.toByteArray();
        }


        // copied from Android StringRequest class
        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            String parsed;
            try {
                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            } catch (UnsupportedEncodingException e) {
                parsed = new String(response.data);
            }
            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
        }

        @Override
        protected void deliverResponse(String response)
        {
            mListener.onResponse(response);
        }
    }*/

    @SuppressWarnings("deprecation")
    public class VideoMultipartRequest extends Request<String> {

        private MultipartEntity entity = new MultipartEntity();

        private static final String FILE_PART_NAME = "video";

        private final Response.Listener<String> mListener;
        private final File file;
        private final HashMap<String, String> params;

        public VideoMultipartRequest(String url, Response.Listener<String> listener, Response.ErrorListener
                errorListener, File file, HashMap<String, String> params) {
            super(Method.POST, url, errorListener);

            mListener = listener;
            this.file = file;
            this.params = params;
            buildMultipartEntity();
        }

        private void buildMultipartEntity() {
            entity.addPart(FILE_PART_NAME, new FileBody(file));
            try {
                for (String key : params.keySet()) {
                    entity.addPart(key, new StringBody(params.get(key)));
                }
            } catch (UnsupportedEncodingException e) {
                VolleyLog.e("UnsupportedEncodingException");
            }
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String,String> headers = new HashMap<>();
            headers.put("Content-Type", "multipart/form-data");
            headers.put("userid", "8");
            headers.put("authorization", "V2kPVYlJIRDmJfeN");
            return headers;
        }

        @Override
        public String getBodyContentType() {
            return entity.getContentType().getValue();
        }

        @Override
        public byte[] getBody() throws AuthFailureError {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                entity.writeTo(bos);
            } catch (IOException e) {
                VolleyLog.e("IOException writing to ByteArrayOutputStream");
            }
            return bos.toByteArray();
        }


        // copied from Android StringRequest class
        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            String parsed;
            try {
                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            } catch (UnsupportedEncodingException e) {
                parsed = new String(response.data);
            }
            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
        }

        @Override
        protected void deliverResponse(String response)
        {
            mListener.onResponse(response);
        }
    }
}